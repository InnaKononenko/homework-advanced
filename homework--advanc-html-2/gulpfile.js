const gulp = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const rename = require("gulp-rename");
const browserSync = require("browser-sync").create();
const tinypng = require("gulp-tinypng");
const autoprefixer = require("gulp-autoprefixer");
const minifyjs = require("gulp-js-minify");
const concatCss = require("gulp-concat-css");
const cleanCSS = require("gulp-clean-css");
const watch = require("gulp-watch");

gulp.task("buildStyle", () => {
  return gulp
    .src("./src/styles-scss/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest("./src/styles-css"));
});
gulp.task("tinypng", () => {
  return gulp
    .src("./src/images/*")
    .pipe(tinypng("Tf3xFMPYlz0r2SsdsXwQtghJcLspQYym"))
    .pipe(gulp.dest("dist/img"));
});
gulp.task("prefixer", () => {
  return gulp
    .src("./src/styles-css/style.css")
    .pipe(
      autoprefixer({
        overrideBrowserList: ["last 2 versions"],
        cascade: false,
      })
    )
    .pipe(gulp.dest("dist"));
});
gulp.task("minify-js", function () {
  return gulp
    .src("./src/js/index.js")
    .pipe(minifyjs())
    .pipe(rename("script.min.js"))
    .pipe(gulp.dest("dist/script"));
});
gulp.task("concatCss", function () {
  return gulp
    .src("src/styles-css/*.css")
    .pipe(concatCss("style.concat.css"))
    .pipe(gulp.dest("src/styles-css"));
});
gulp.task("clean-css", () => {
  return gulp
    .src("src/styles-css/style.concat.css")
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(rename("style.min.css"))
    .pipe(gulp.dest("dist/style"));
});

gulp.task(
  "build",
  gulp.series([
    "buildStyle",
    "prefixer",
    "tinypng",
    "minify-js",
    "concatCss",
    "clean-css",
  ])
);

gulp.task("dev", () => {
  browserSync.init({
    server: "./",
  });
  gulp.watch(
    "./src/styles-scss/**/**.scss",
    gulp.series("buildStyle", "prefixer", "concatCss", "clean-css")
  );
  gulp.watch("./src/js/index.js", gulp.series("minify-js"));
  gulp.watch("./index.html").on("change", browserSync.reload);
  gulp.watch("./dist/style/style.min.css").on("change", browserSync.reload);
  gulp.watch("./dist/script/script.min.js").on("change", browserSync.reload);
});
