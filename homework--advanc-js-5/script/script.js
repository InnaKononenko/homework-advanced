"use strict";
const API = "https://ajax.test-danit.com/api/json/";
const cardForm = document.querySelector(".cards");

const sendRequest = async (entity, method = "GET", config) => {
  return await fetch(`${API}${entity}`, {
    method,
    ...config,
  }).then((response) => {
    if (response.ok) {
      if (method === "GET" || method === "POST" || method === "PUT") {
        return response.json();
      }
      return response;
    } else {
      return new Error("error");
    }
  });
};
class Card {
  constructor(name, username, email, title, body) {
    this.name = name;
    this.username = username;
    this.email = email;
    this.title = title;
    this.body = body;
  }

  deletePost() {
    const btnDelete = document.querySelector("#delete");
    btnDelete.addEventListener("click", function () {
      if (confirm("Are you sure you want to delete?")) {
        const postId = btnDelete.closest(".user-card").id;
        delPost(postId).then((response) => {
          if (response.status === 200) {
            btnDelete.closest(".user-card").remove();
          }
        });
      }
    });
  }
}

const createCard = () => {
  sendRequest("/users").then((arrUser) => {
    arrUser.forEach(({ id, name, username, email }) => {
      const card = new Card(name, username, email);
      getPosts(id, card);
    });
  });
};

const getPosts = (idUser, card) =>
  sendRequest(`posts/`).then((arrPosts) => {
    arrPosts.forEach(({ id, userId, title, body }) => {
      if (idUser === userId) {
        card.title = title;
        card.body = body;
        card.id = userId;
        renderData(card, id);
      }
    });
  });

const delPost = (id) => sendRequest(`/posts/${id}`, "DELETE");

const renderData = (card, id) => {
  let time = new Date().toLocaleString();
  cardForm.insertAdjacentHTML(
    "afterbegin",
    `  <div class="user-card" id="${id}">
           <div class="user-card__info">
              <p id="name">${card.name}  ${card.username}</p>
              <p id="email">${card.email}</p>
              <button id="delete">Delete</button>
          </div>
           <div class="user-card__post">
              <h4>${card.title}</h4>
              <p>${card.body}</p>
              <p id="time">${time}</p>
          </div>
        </div>`
  );
  card.deletePost();
};

createCard();
