"use strict";
const API = "https://api.ipify.org/?format=json";
const infoAPI = "http://ip-api.com/json/";
const findButton = document.querySelector("#find-button");
const findInfo = document.querySelector(".find-info");

const sendRequest = async (url) => {
  const response = await fetch(url);
  const result = response.json();
  return result;
};

async function getIp() {
  return sendRequest(API);
}

async function getInfo() {
  const { ip } = await getIp();
  sendRequest(`${infoAPI}${ip}`).then((data) => {
    renderData(data);
  });
}

const renderData = ({ timezone, country, regionName, city }) => {
  if (findInfo.innerHTML === "") {
    findInfo.insertAdjacentHTML(
      "afterbegin",
      `   <h4>I know all about you!</h4>
        <li>Continent: ${timezone.split("/")[0]}</li>
        <li>Country: ${country}</li>
        <li>Region: ${regionName}</li>
        <li>City: ${city}</li>
  `
    );
  }
};

findButton.addEventListener("click", () => {
  getInfo();
});
