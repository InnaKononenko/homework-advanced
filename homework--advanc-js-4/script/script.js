"use strict";

const API = "https://ajax.test-danit.com/api/swapi/films";
const filmsDiv = document.createElement("div");
document.body.append(filmsDiv);

const sendRequest = async (url) => {
  const response = await fetch(url);
  const result = response.json();
  return result;
};

const getFilms = (url) => {
  sendRequest(url)
    .then((data) => {
      data.forEach(({ episodeId, name, openingCrawl }) => {
        filmsDiv.insertAdjacentHTML(
          "afterbegin",
          `<div class="films">
           <h3>${name}</h3>
           <h5 id="actors">Actors: <h5>
           <div class="loader"></div>
           <div class="id"></div>
             <ul>
               <li>Episode number: ${episodeId}</li>
               <li>${openingCrawl}</li>

             </ul>
           </div>`
        );
      });
      getCharacters(data);
    })
    .catch((error) => {
      console.log(error.message);
    });
};

function getCharacters(arr) {
  const divId = document.querySelectorAll(".id");
  arr.forEach(({ characters, id }) => {
    divId.forEach((element, index) => {
      if (id == index + 1) {
        characters.forEach((link) => {
          getNames(link, element);
        });
      }
    });
  });
}
async function getNames(link, element) {
  await sendRequest(link)
    .then((actors) => {
      element.insertAdjacentHTML("afterbegin", `<p>${actors.name}</p>, `);
    })
    .catch((error) => {
      console.log(error.massage);
    })
    .finally(() =>
      document.querySelectorAll(".loader").forEach((el) => {
        el.style.display = "none";
      })
    );
}
getFilms(API);
