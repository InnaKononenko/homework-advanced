class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  getName() {
    return `${this.name}`;
  }
  setName(newName) {
    this.name = newName;
  }
  getAge() {
    return `${this.name}`;
  }
  setAge(newAge) {
    this.age = newAge;
  }
  getSalary() {
    return `${this.name}`;
  }
  setSalary(newSalary) {
    this.salary = newSalary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, langs) {
    super(name, age, salary);
    this.langs = langs;
  }
  getSalary() {
    return this.salary * 3;
  }
}
const programmer = new Programmer("Inna", "24", "2000", ["eng", "ua"]);
const programmerTwo = new Programmer("Bogdan", "27", "3000", ["fra", "ua"]);
const programmerThree = new Programmer("Elena", "35", "2700", ["pln", "ua"]);
console.log(programmer);
console.log(programmerTwo);
console.log(programmerThree);
