"use strict";
//1 task
const clients1 = [
  "Гилберт",
  "Сальваторе",
  "Пирс",
  "Соммерс",
  "Форбс",
  "Донован",
  "Беннет",
];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
const allClients = [...new Set([...clients1, ...clients2])];

//2 task
const characters = [
  {
    name: "Елена",
    lastName: "Гилберт",
    age: 17,
    gender: "woman",
    status: "human",
  },
  {
    name: "Кэролайн",
    lastName: "Форбс",
    age: 17,
    gender: "woman",
    status: "human",
  },
  {
    name: "Аларик",
    lastName: "Зальцман",
    age: 31,
    gender: "man",
    status: "human",
  },
  {
    name: "Дэймон",
    lastName: "Сальваторе",
    age: 156,
    gender: "man",
    status: "vampire",
  },
  {
    name: "Ребекка",
    lastName: "Майклсон",
    age: 1089,
    gender: "woman",
    status: "vempire",
  },
  {
    name: "Клаус",
    lastName: "Майклсон",
    age: 1093,
    gender: "man",
    status: "vampire",
  },
];
const charactersShortInfo = [];

function createNewArr(arr) {
  arr.forEach((obj) => {
    const { name, lastName, age, gender, status } = obj;
    const newObj = { name, lastName, age };
    newObj.__proto__ = obj;
    charactersShortInfo.push(newObj);
  });
}
console.log(charactersShortInfo);
createNewArr(characters);
//3 task
const user1 = {
  name: "John",
  years: 30,
};
const { name: firstName, years: age, isAdmin = false } = user1;
console.log(firstName, age, isAdmin);
//4 task
const satoshi2020 = {
  name: "Nick",
  surname: "Sabo",
  age: 51,
  country: "Japan",
  birth: "1979-08-21",
  location: {
    lat: 38.869422,
    lng: 139.876632,
  },
};

const satoshi2019 = {
  name: "Dorian",
  surname: "Nakamoto",
  age: 44,
  hidden: true,
  country: "USA",
  wallet: "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa",
  browser: "Chrome",
};

const satoshi2018 = {
  name: "Satoshi",
  surname: "Nakamoto",
  technology: "Bitcoin",
  country: "Japan",
  browser: "Tor",
  birth: "1975-04-05",
};
const fullProfile = { ...satoshi2018, ...satoshi2019, ...satoshi2020 };
//5 task
const books = [
  {
    name: "Harry Potter",
    author: "J.K. Rowling",
  },
  {
    name: "Lord of the rings",
    author: "J.R.R. Tolkien",
  },
  {
    name: "The witcher",
    author: "Andrzej Sapkowski",
  },
];

const bookToAdd = {
  name: "Game of thrones",
  author: "George R. R. Martin",
};
const allBooks = [...books, bookToAdd];
// 6 task
const employee = {
  name: "Vitalii",
  surname: "Klichko",
};
const copyEmployee = { ...employee, salary: 2000, age: 20 };
console.log(copyEmployee);
//7 task
const array = ["value", () => "showValue"];
const [value, showValue] = array;

alert(value); // має бути виведено 'value'
alert(showValue()); // має бути виведено 'showValue'
