"use strict";

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж та м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const parentElem = document.querySelector("#root");
const bookList = document.createElement("ul");
bookList.className = "bookList";
parentElem.append(bookList);

function sortBook(arr, obj) {
  const property = ["author", "name", "price"];
  property.filter((el) => {
    if (!obj.hasOwnProperty(el)) {
      throw `Object number ${
        arr.indexOf(obj) + 1
      } does not have parameter: ${el}`;
    }
  });
}

function addList(arr) {
  arr.forEach((el) => {
    const bookItem = document.createElement("li");
    const objectList = document.createElement("ul");
    bookItem.append(objectList);
    try {
      sortBook(books, el);
      for (const [key, value] of Object.entries(el)) {
        bookList.append(bookItem);
        const objectLi = document.createElement("li");
        objectList.append(objectLi);
        objectLi.innerText = `${key}: ${value}`;
      }
    } catch (error) {
      console.log(error);
    }
  });
}
addList(books);
